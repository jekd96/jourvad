package app.auth.service;

import app.auth.domain.Group;
import app.auth.domain.Student;
import app.auth.dto.StudentDTO;
import app.auth.repository.StudentRepository;
import org.jooq.DSLContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StudentService {

    @Autowired
    private StudentRepository studentRepository;

    public Student createStudent(StudentDTO studentDTO) {
        Student student = new Student();
        Group group = new Group();
        group.setId(studentDTO.getGroupId());
        student.setGroup(group);
        student.setName(studentDTO.getName());
        studentRepository.save(student);
        return student;
    }

    public List<Student> findStudentByGroupId(Long groupId) {
        return studentRepository.findStudentByGroup(groupId);
    }

    public List<Student> findAll() {
        return studentRepository.findAll();
    }

    public void deleteStudent(Long studentId) {
        studentRepository.delete(studentId);
    }

    public Student updateStudent(StudentDTO studentDTO) {
        Student student = new Student();
        student.setId(studentDTO.getId());
        Group group = new Group();
        group.setId(studentDTO.getGroupId());
        student.setGroup(group);
        student.setName(studentDTO.getName());
        studentRepository.save(student);
        return student;
    }

}

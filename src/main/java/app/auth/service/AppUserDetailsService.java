package app.auth.service;

import app.auth.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AccountStatusUserDetailsChecker;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import app.auth.domain.User;

/**
 * Created by almagnit on 11.09.2016.
 */
@Service("appUserDetailsService")
public class AppUserDetailsService implements UserDetailsService {

    private final AccountStatusUserDetailsChecker detailsChecker = new AccountStatusUserDetailsChecker();

    @Autowired
    UserRepository userRepository;

    @Override
    public User loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findOneByUsername(username);
        if(user == null) throw new UsernameNotFoundException("User.username " + username + " not found" );
        detailsChecker.check(user);
        return user;
    }

}

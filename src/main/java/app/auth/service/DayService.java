package app.auth.service;

import app.auth.domain.Day;
import app.auth.repository.DayRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DayService {

    @Autowired
    private DayRepository dayRepository;

    public List<Day> getDays() {
        return dayRepository.findAll();
    }

}

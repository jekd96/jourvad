package app.auth.service;

import app.auth.domain.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;


@Service
public class VdnUserAuth {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private AppUserDetailsService userDetailsService;

    @Autowired
    private UserSessionService userSessionService;

    private static final String AUTH_HEADER_NAME = "X-AUTH-TOKEN";

    public String login(String username, String password, HttpServletResponse response) throws AuthenticationException  {
//        VaadinService.getCurrentResponse().setHeader("Access-Control-Allow-Origin", "*");
//        VaadinService.getCurrentResponse().setHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, X-AUTH-TOKEN");
//        VaadinService.getCurrentResponse().setHeader("Access-Control-Expose-Headers", "X-AUTH-TOKEN");
//        VaadinService.getCurrentResponse().setHeader("Access-Control-Allow-Methods", "POST, PUT, GET, OPTIONS, DELETE");

        final UsernamePasswordAuthenticationToken loginToken = new UsernamePasswordAuthenticationToken(
                username.toLowerCase().trim(), password);
        Authentication authentication;

        authentication = authenticationManager.authenticate(loginToken);

        //end attemptAuthentication() -authentication

        if (authentication!=null && authentication.isAuthenticated()) {
            // Lookup the complete User object from the database and create an Authentication for it
            final User authenticatedUser = userDetailsService.loadUserByUsername(authentication.getName());
            final UserAuthentication userAuthentication = new UserAuthentication(authenticatedUser);
            // Add the custom token as HTTP header to the response
            userSessionService.addAuthentication(response, userAuthentication);
            // Add the authentication to the Security context
            SecurityContextHolder.getContext().setAuthentication(userAuthentication);


//            String xAuthToken = ((HttpServletResponse) VaadinService.getCurrentResponse()).getHeader(AUTH_HEADER_NAME);
//            Cookie xAuthTokenCookie = new Cookie("X-AUTH-TOKEN", xAuthToken);
//            xAuthTokenCookie.setPath(VaadinService.getCurrentRequest().getContextPath());
//            VaadinService.getCurrentResponse().addCookie(xAuthTokenCookie);
            return "xAuthToken";

        } else {
            return null;
        }
    }

}

package app.auth.utils.jooq;

import app.auth.domain.Student;
import app.auth.domain.User;
import app.auth.utils.jooq.mapper.StudentRecordMapper;
import org.jooq.Record;
import org.jooq.RecordMapper;
import org.jooq.RecordMapperProvider;
import org.jooq.RecordType;
import org.jooq.impl.DefaultRecordMapper;

public class AppRecordMapperProvider implements RecordMapperProvider {
    @Override
    public <R extends Record, E> RecordMapper<R, E> provide(RecordType<R> recordType, Class<? extends E> type) {

        if (type == Student.class) {
            return new StudentRecordMapper();
        }

        // Fall back to jOOQ's DefaultRecordMapper, which maps records onto
        // POJOs using reflection.
        return new DefaultRecordMapper(recordType, type);
    }

    public class UserWithAuthorities extends User {
    }

}

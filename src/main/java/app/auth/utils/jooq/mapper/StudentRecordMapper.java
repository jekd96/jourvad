package app.auth.utils.jooq.mapper;

import app.auth.domain.Student;
import app.auth.utils.jooq.tables.records.StudentsRecord;
import org.jooq.Record;
import org.jooq.RecordMapper;

public class StudentRecordMapper <R extends Record, E> implements RecordMapper<R, Student> {

    @Override
    public Student map(R r) {
        StudentsRecord studentsRecord = r.into(StudentsRecord.class);
        Student student = new Student();
        student.setId(studentsRecord.getId());
        student.setName(studentsRecord.getName());
        return student;

    }
}

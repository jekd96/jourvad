package app.auth.domain;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "days")
@Data
@Builder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor
@ToString
@EqualsAndHashCode(callSuper = true)
public class Day extends AppAbstractPersistable {

    @Column(name = "title")
    private String title;

    @Column(name = "abbreviated")
    private String abbreviated;

}

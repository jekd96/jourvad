package app.auth.domain;


public enum AuthorityType {

    ROLE_ADMIN,
    ROLE_USER,
    ROLE_PRAEPPSTOR

}

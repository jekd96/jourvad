package app.auth.domain;

import lombok.*;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

import static javax.persistence.FetchType.EAGER;

@Entity
@Table(name = "teachers")
@Data
@Builder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor
@ToString
@EqualsAndHashCode(callSuper = true)
public class Teacher extends AppAbstractPersistable {

    @ManyToMany(cascade = CascadeType.MERGE, fetch = EAGER)
    @JoinTable(name="teacher_lessons",
            joinColumns = {
                    @JoinColumn(name="teacher_id", referencedColumnName = "id")
            },
            inverseJoinColumns = {
                    @JoinColumn(name="lesson_id", referencedColumnName = "id")
            })
    @Singular("lessons")
    private Set<Lesson> lessons = new HashSet<>();

    @Column(name = "name")
    private String name;

}

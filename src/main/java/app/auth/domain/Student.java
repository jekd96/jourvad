package app.auth.domain;

import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "students")
@Data
@Builder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor
@ToString
@EqualsAndHashCode(callSuper = true)
public class Student extends AppAbstractPersistable {

    @Column(name = "name")
    private String name;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "group_id")
    private Group group;

    @Column(name = "student_type")
    @Enumerated(EnumType.STRING)
    private StudentType studentType;
}

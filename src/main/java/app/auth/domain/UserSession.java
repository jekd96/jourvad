package app.auth.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;


@Entity
@Table(name = "user_sessions")
@Data
@EqualsAndHashCode(callSuper = true)
public class UserSession extends AppAbstractPersistable {

    protected Long expires;

    private String token;

    @ManyToOne
    private User user;

}

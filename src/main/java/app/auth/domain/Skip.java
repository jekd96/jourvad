package app.auth.domain;

import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "skips")
@Builder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor
@ToString
@EqualsAndHashCode(callSuper = true)
public class Skip extends AppAbstractPersistable {

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "student_id")
    private Student student;

    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    private SkipStatus skipStatus;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "subject_id")
    private Subject subject;


    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public SkipStatus getSkipStatus() {
        return skipStatus;
    }

    public void setSkipStatus(SkipStatus skipStatus) {
        this.skipStatus = skipStatus;
    }

    public Subject getSubject() {
        return subject;
    }

    public void setSubject(Subject subject) {
        this.subject = subject;
    }
}

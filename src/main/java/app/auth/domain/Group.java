package app.auth.domain;

import lombok.*;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

import static javax.persistence.FetchType.EAGER;

@Entity
@Table(name = "groups")
@Data
@Builder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor
@ToString
@EqualsAndHashCode(callSuper = true)
public class Group extends AppAbstractPersistable {

    @ManyToMany(cascade = CascadeType.MERGE, fetch = EAGER)
    @JoinTable(name="group_lessons",
            joinColumns = {
                    @JoinColumn(name="group_id", referencedColumnName = "id")
            },
            inverseJoinColumns = {
                    @JoinColumn(name="lesson_id", referencedColumnName = "id")
            })
    @Singular("lessons")
    private Set<Lesson> lessons = new HashSet<>();

    @Column(name = "title")
    private String title;

}

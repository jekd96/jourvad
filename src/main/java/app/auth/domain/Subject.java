package app.auth.domain;

import lombok.*;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "subjects")
@Builder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class Subject extends AppAbstractPersistable {

    /*
    * Дата проведенной темы
    */
    @Column(name = "created_date")
    public Date createdDate;


    /*
    * Тема
    */
    @Column(name = "topic")
    public String topic;

    /*
    * Преподователь который провел тему
    */
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "teacher_id")
    public Teacher teacher;

    /*
    * Статус пары
    */
    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    public SubjectStatus subjectStatus;

    /*
    * Группа у которой проведена пара
    */
    @ManyToOne
    @JoinColumn(name = "group_id")
    public Group group;

    /*
    * Урок
    */
    @ManyToOne
    @JoinColumn(name = "lesson_id")
    public Lesson lesson;

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public Teacher getTeacher() {
        return teacher;
    }

    public void setTeacher(Teacher teacher) {
        this.teacher = teacher;
    }

    public SubjectStatus getSubjectStatus() {
        return subjectStatus;
    }

    public void setSubjectStatus(SubjectStatus subjectStatus) {
        this.subjectStatus = subjectStatus;
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    public Lesson getLesson() {
        return lesson;
    }

    public void setLesson(Lesson lesson) {
        this.lesson = lesson;
    }
}


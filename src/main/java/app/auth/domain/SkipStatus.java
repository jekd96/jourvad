package app.auth.domain;

public enum SkipStatus {
    /*
    * Присуствовал
    */
    ATTENDED,

    /*
    * Отсуствовал
    */
    NOT_ATTENDED
}

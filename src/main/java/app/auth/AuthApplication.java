package app.auth;

import app.auth.domain.Authority;
import app.auth.domain.AuthorityType;
import app.auth.domain.User;
import app.auth.domain.UserProfile;
import app.auth.repository.UserProfileRespository;
import app.auth.repository.UserRepository;
import com.vaadin.spring.annotation.EnableVaadin;
import com.vaadin.spring.boot.annotation.EnableVaadinServlet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.annotation.PostConstruct;
import java.util.HashSet;

@SpringBootApplication(
		scanBasePackages = {
				"app.auth.configuration",
				"app.auth.web",
				"app.auth.service",
				"app.auth.utils",
				"app.auth.repository",
				"app.auth.ui",
				"app.auth.ui.view"
		}
)
@EnableJpaAuditing
@EnableTransactionManagement
@EnableJpaRepositories(basePackages = "app.auth.repository")
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true, prePostEnabled = true)
@EnableVaadin
@EnableVaadinServlet
public class AuthApplication {

	@Autowired
	PasswordEncoder passwordEncoder;

	@Autowired
	private UserRepository userRepository;
	@Autowired
	private UserProfileRespository userProfileRespository;

	@PostConstruct
	public void init() {
		//TODO ВРЕМЕНОО!!!!!! ПЕРЕДЕЛАТЬ!!
		if (userRepository.count() == 0) {
			User user = new User();
			user.setUsername("jekd96@mailinator.com");
			user.setEmail("jekd96@mailinator.com");
			user.setEnabled(true);
			user.setPassword(passwordEncoder.encode("adminpass"));
			user.setAuthorities(new HashSet<>());
			user.addAuthority(new Authority(AuthorityType.ROLE_ADMIN));
			user.addAuthority(new Authority(AuthorityType.ROLE_USER));
			userProfileRespository.save(new UserProfile("Федор","Федоров"));
		//	user.setUserProfile(new UserProfile("Евгений",""));
			userRepository.save(user);

			User user2 = new User();
			user2.setUsername("prtt96@mailinator.com");
			user2.setEmail("prtt96@mailinator.com");
			user2.setEnabled(true);
			user2.setPassword(passwordEncoder.encode("adminpass"));
			user2.setAuthorities(new HashSet<>());
			user2.addAuthority(new Authority(AuthorityType.ROLE_PRAEPPSTOR));
		//	user2.setUserProfile(new UserProfile("Иван", "Иванов"));
			userProfileRespository.save(new UserProfile("Иван","Иванов"));
			userRepository.save(user2);
		}
	}

	public static void main(String[] args) {
		SpringApplication.run(AuthApplication.class, args);
	}
}

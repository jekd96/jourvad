package app.auth.ui.view.auth;

import app.auth.service.VdnUserAuth;
import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.Page;
import com.vaadin.server.Responsive;
import com.vaadin.server.VaadinService;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.ValoTheme;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletResponse;

@SpringView(name = "signin")
public class SigninView extends VerticalLayout implements View {

    @Autowired
    VdnUserAuth vdnUserAuth;

    @PostConstruct
    public void init() {
        setSizeFull();

        Component loginForm = buildLoginForm();
        addComponent(loginForm);
        setComponentAlignment(loginForm, Alignment.MIDDLE_CENTER);

    }

     private Component buildLoginForm() {
        final VerticalLayout loginPanel = new VerticalLayout();
        loginPanel.setSizeUndefined();
        loginPanel.setSpacing(true);
        Responsive.makeResponsive(loginPanel);
        loginPanel.addStyleName("login-panel");

        loginPanel.addComponent(buildLabels());
        loginPanel.addComponent(buildFields());
        loginPanel.addComponent(new CheckBox("Remember me", true));
        return loginPanel;
    }

    private Component buildFields() {
        HorizontalLayout fields = new HorizontalLayout();
        fields.setSpacing(true);
        fields.addStyleName("fields");

        final TextField username = new TextField("Имя");
        username.setIcon(FontAwesome.USER);
        username.addStyleName(ValoTheme.TEXTFIELD_INLINE_ICON);
        username.setValue("admin");

        final PasswordField password = new PasswordField("Пароль");
        password.setIcon(FontAwesome.LOCK);
        password.addStyleName(ValoTheme.TEXTFIELD_INLINE_ICON);
       // password.setValue("admin");

        final Button signin = new Button("Вход");
        signin.addStyleName(ValoTheme.BUTTON_PRIMARY);
        signin.setClickShortcut(KeyCode.ENTER);
        signin.focus();

        fields.addComponents(username, password, signin);
        fields.setComponentAlignment(signin, Alignment.BOTTOM_LEFT);

        signin.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(final Button.ClickEvent event) {
                if (!login(username.getValue(),password.getValue())){
                    username.focus();
                }
                else {
                    Page.getCurrent().setLocation("/admin");
                }
            }
        });
        return fields;
    }

    private Component buildLabels() {
        CssLayout labels = new CssLayout();
        labels.addStyleName("labels");

        Label welcome = new Label("Добро пожаловать!");
        welcome.setSizeUndefined();
        welcome.addStyleName(ValoTheme.LABEL_H4);
        welcome.addStyleName(ValoTheme.LABEL_COLORED);
        labels.addComponent(welcome);

        Label title = new Label("Auth App");
        title.setSizeUndefined();
        title.addStyleName(ValoTheme.LABEL_H3);
        title.addStyleName(ValoTheme.LABEL_LIGHT);
        labels.addComponent(title);
        return labels;
    }


    private boolean login(String username, String password)  {
        VaadinService.getCurrentResponse().setHeader("Access-Control-Allow-Origin", "*");
        VaadinService.getCurrentResponse().setHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, X-AUTH-TOKEN");
        VaadinService.getCurrentResponse().setHeader("Access-Control-Expose-Headers", "X-AUTH-TOKEN");
        VaadinService.getCurrentResponse().setHeader("Access-Control-Allow-Methods", "POST, PUT, GET, OPTIONS, DELETE");
        HttpServletResponse response = (HttpServletResponse) VaadinService.getCurrentResponse();
        vdnUserAuth.login(username,password, response);
        String xAuthToken = response.getHeader("X-AUTH-TOKEN");
        if (xAuthToken!=null){
            Page.getCurrent().getJavaScript().execute("document.cookie='X-AUTH-TOKEN="+xAuthToken+"; path=/'");
            showNotification(new Notification("Вход успешный",xAuthToken));
            return true;
        }else {
            showNotification(new Notification("Ошибка входа",
                    "Пожалуйста, проверьте имя и пароль и попробуйте снова.",
                    Notification.Type.HUMANIZED_MESSAGE));
            return false;


        }
    }
    private void showNotification(Notification notification) {
        // keep the notification visible a little while after moving the
        // mouse, or until clicked
        notification.setDelayMsec(2000);
        notification.show(Page.getCurrent());
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {

    }
}

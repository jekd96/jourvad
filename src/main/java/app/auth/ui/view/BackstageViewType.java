package app.auth.ui.view;

import com.vaadin.server.FontAwesome;
import com.vaadin.server.Resource;

public enum BackstageViewType {
    LEARNS("learn", "Предметы", FontAwesome.APPLE),
    TIMETABLES("timetable","Расписание", FontAwesome.APPLE)
    ;
    private final String viewName;
    private final Resource icon;
    private final String caption;

    BackstageViewType(String viewName, String caption, Resource icon) {
        this.viewName = viewName;
        this.icon = icon;
        this.caption = caption;
    }

    public String getViewName() {
        return viewName;
    }

    public String getCaption() {
        return caption;
    }

    public Resource getIcon() {
        return icon;
    }

    public static BackstageViewType getByViewName(final String viewName) {
        BackstageViewType result = null;
        for (BackstageViewType viewType : values()) {
            if (viewType.getViewName().equals(viewName)) {
                result = viewType;
                break;
            }
        }
        return result;
    }
}

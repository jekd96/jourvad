package app.auth.ui.view;


import app.auth.domain.User;
import app.auth.service.UserSessionService;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.Page;
import com.vaadin.server.ThemeResource;
import com.vaadin.server.VaadinService;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.*;
import com.vaadin.ui.MenuBar.Command;
import com.vaadin.ui.MenuBar.MenuItem;
import com.vaadin.ui.themes.ValoTheme;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetailsService;

import javax.servlet.http.HttpServletRequest;


/**
 * A responsive menu component providing user information and the controls for
 * primary navigation between the views.
 */
@SuppressWarnings({ "serial", "unchecked" })
public final class MainMenu extends CustomComponent {


    private UserSessionService sessionService;
    private UserDetailsService userDetailsService;

    public static final String ID = "journal-menu";
    private static final String STYLE_VISIBLE = "valo-menu-visible";
    private Label learnsBadge;
    private Label skipBadge;
    private Label timetableBadge;
    private MenuItem settingsItem;

    public MainMenu(UserSessionService sessionService, UserDetailsService userDetailsService) {
        this.sessionService = sessionService;
        this.userDetailsService = userDetailsService;
        setPrimaryStyleName("valo-menu");
        setId(ID);
        setSizeUndefined();
        setCompositionRoot(buildContent());
    }

    private Component buildContent() {
        final CssLayout menuContent = new CssLayout();
        menuContent.addStyleName("sidebar");
        menuContent.addStyleName(ValoTheme.MENU_PART);
        menuContent.addStyleName("no-vertical-drag-hints");
        menuContent.addStyleName("no-horizontal-drag-hints");
        menuContent.setWidth(null);
        menuContent.setHeight("100%");

        menuContent.addComponent(buildTitle());
        menuContent.addComponent(buildUserMenu());
        menuContent.addComponent(buildToggleButton());
        menuContent.addComponent(buildMenuItems());

        return menuContent;
    }

    private Component buildTitle() {
        Label logo = new Label("<strong>Журналы</strong>",
                ContentMode.HTML);
        logo.setSizeUndefined();
        HorizontalLayout logoWrapper = new HorizontalLayout(logo);
        logoWrapper.setComponentAlignment(logo, Alignment.MIDDLE_CENTER);
        logoWrapper.addStyleName("valo-menu-title");
        return logoWrapper;
    }

    private User getCurrentUser() {
        Authentication authentication= sessionService.getAuthentication((HttpServletRequest) VaadinService.getCurrentRequest());
        User user=null;
        if (authentication!=null && authentication.isAuthenticated()) {
            // Lookup the complete User object from the database and create an Authentication for it
            user = (User)userDetailsService.loadUserByUsername(authentication.getName());
        }
        return user ;
    }

    private Component buildUserMenu() {
        final MenuBar settings = new MenuBar();
        settings.addStyleName("user-menu");
        final User user = getCurrentUser();
        settingsItem = settings.addItem("",
                new ThemeResource("img/profile-pic-300px.jpg"), null);
        updateUserName(user);
        settingsItem.addItem("Edit Profile", new Command() {
            @Override
            public void menuSelected(final MenuItem selectedItem) {
//                ProfilePreferencesWindow.open(user, false);
            }
        });
        settingsItem.addItem("Preferences", new Command() {
            @Override
            public void menuSelected(final MenuItem selectedItem) {
//                ProfilePreferencesWindow.open(user, true);
            }
        });
        settingsItem.addSeparator();
        settingsItem.addItem("Sign Out", new Command() {
            @Override
            public void menuSelected(final MenuItem selectedItem) {
                Page.getCurrent().setLocation("/auth#!signout");
            }
        });
        return settings;
    }

    private Component buildToggleButton() {
        Button valoMenuToggleButton = new Button("Menu", new Button.ClickListener() {
            @Override
            public void buttonClick(final Button.ClickEvent event) {
                if (getCompositionRoot().getStyleName()
                        .contains(STYLE_VISIBLE)) {
                    getCompositionRoot().removeStyleName(STYLE_VISIBLE);
                } else {
                    getCompositionRoot().addStyleName(STYLE_VISIBLE);
                }
            }
        });
        valoMenuToggleButton.setIcon(FontAwesome.LIST);
        valoMenuToggleButton.addStyleName("valo-menu-toggle");
        valoMenuToggleButton.addStyleName(ValoTheme.BUTTON_BORDERLESS);
        valoMenuToggleButton.addStyleName(ValoTheme.BUTTON_SMALL);
        return valoMenuToggleButton;
    }

    private Component buildMenuItems() {
        CssLayout menuItemsLayout = new CssLayout();
        menuItemsLayout.addStyleName("valo-menuitems");
        //learns button
        Component learnsMenuItemComponent = new ValoMenuItemButton(BackstageViewType.LEARNS);
        learnsBadge = new Label();
        learnsBadge.setId(BackstageViewType.LEARNS.getViewName());
        learnsMenuItemComponent = buildBadgeWrapper(learnsMenuItemComponent, learnsBadge);
        menuItemsLayout.addComponent(learnsMenuItemComponent);


        Component timetableMenuItemComponent = new ValoMenuItemButton(BackstageViewType.TIMETABLES);
        timetableBadge = new Label();
        timetableBadge.setId(BackstageViewType.TIMETABLES.getViewName());
        timetableMenuItemComponent = buildBadgeWrapper(timetableMenuItemComponent, timetableBadge);
        menuItemsLayout.addComponent(timetableMenuItemComponent);

        return menuItemsLayout;

    }

    private Component buildBadgeWrapper(final Component menuItemButton,
                                        final Component badgeLabel) {
        CssLayout backstageWrapper = new CssLayout(menuItemButton);
        backstageWrapper.addStyleName("badgewrapper");
        backstageWrapper.addStyleName(ValoTheme.MENU_ITEM);
        badgeLabel.addStyleName(ValoTheme.MENU_BADGE);
        badgeLabel.setWidthUndefined();
        badgeLabel.setVisible(false);
        backstageWrapper.addComponent(badgeLabel);
        return backstageWrapper;
    }

    @Override
    public void attach() {
        super.attach();
    }

    public void postViewChange() {
        // After a successful view change the menu can be hidden in mobile view.
        getCompositionRoot().removeStyleName(STYLE_VISIBLE);
    }


    public void updateUserName(User user) {
        settingsItem.setText(user.getUsername() + "(" + user.getEmail()+")");
    }

    public final class ValoMenuItemButton extends Button {

        private static final String STYLE_SELECTED = "selected";

        private final BackstageViewType view;

        public ValoMenuItemButton(final BackstageViewType view) {
            this.view = view;
            setPrimaryStyleName("valo-menu-item");
            setIcon(view.getIcon());
            setCaption(view.getCaption());
            addClickListener(new ClickListener() {
                @Override
                public void buttonClick(final ClickEvent event) {
                    UI.getCurrent().getNavigator()
                            .navigateTo(view.getViewName());
                }
            });

        }

    }
}

package app.auth.ui.view.admin;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.VerticalLayout;

@SpringView(name = SkipView.VIEW_NAME)
public class SkipView extends VerticalLayout implements View {

    public static final String VIEW_NAME = "skip";


    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {

    }
}


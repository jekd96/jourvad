package app.auth.ui.view.admin;

import app.auth.domain.*;
import app.auth.repository.UserProfileRespository;
import app.auth.repository.UserRepository;
import com.vaadin.data.Item;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.*;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@SpringView(name = PraepostorView.VIEW_NAME)
public class PraepostorView extends VerticalLayout implements View {

    public static final String VIEW_NAME = "praepostor";
    @Autowired
    private UserProfileRespository userProfileRespository;
    private BeanItemContainer<UserProfile> dataSource;
    private final LinkedHashMap<String, String> columns = new LinkedHashMap<>();

    @PostConstruct
    public void init() {

        //TODO это просто ужас...((((
        columns.put("firstName", "Имя");
        columns.put("lastName", "Фамилия");
        dataSource= new BeanItemContainer(UserProfile.class, userProfileRespository.findAll());
        addComponent( getCustomersGrid());

    }

    private Component getCustomersGrid(){


        VerticalLayout content = new VerticalLayout();

        content.setSizeFull();
        Grid grid = new Grid();
        content.addComponent(grid);
        grid.setSizeFull();
        grid.setColumns(columns.keySet().toArray());
        for(Map.Entry<String, String> row : columns.entrySet()) {
            grid.getColumn(row.getKey()).setHeaderCaption(row.getValue());
        }
        grid.setContainerDataSource(dataSource);

        grid.addSelectionListener(selectionEvent -> { // Java 8
            // Get selection from the selection model
            Object selected = ((Grid.SingleSelectionModel)
                    grid.getSelectionModel()).getSelectedRow();

            if (selected != null){
                Item item =grid.getContainerDataSource().getItem(selected);
                final Window window = new Window("Редактирование");
                window.setWidth(300.0f, Unit.PIXELS);
                final FormLayout formContent = new FormLayout();
                window.setContent(formContent);
                window.center();
                formContent.addComponent(new TextField("Имя", item.getItemProperty("firstName")));
                formContent.addComponent(new TextField("Фамилия", item.getItemProperty("lastName")));
                UI.getCurrent().addWindow(window);

                Notification.show("Selected name = \"" +
                        item.getItemProperty("title")+"\" id=\""+
                        item.getItemProperty("id")+"\""
                );

            }
            else
                Notification.show("Nothing selected");
        });
        return  content;
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {
    }

}

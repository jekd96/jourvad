package app.auth.ui.view.admin;

import app.auth.domain.Group;
import app.auth.domain.Lesson;
import app.auth.repository.GroupRepository;
import com.vaadin.data.Item;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.*;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;
import java.util.LinkedHashMap;
import java.util.Map;

@SpringView(name = TimetableView.VIEW_NAME)
public class TimetableView extends VerticalLayout implements View {

    public static final String VIEW_NAME = "timetable";


    @Autowired
    private GroupRepository groupRepository;


    private BeanItemContainer<Group> groupData;

    private final LinkedHashMap<String, String> columns = new LinkedHashMap<>();

    @PostConstruct
    public void init() {

    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {
    }

}

package app.auth.ui.view.admin;


import app.auth.domain.Lesson;
import app.auth.service.LessonService;
import com.vaadin.data.Item;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Component;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Notification;
import com.vaadin.ui.VerticalLayout;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;
import java.util.LinkedHashMap;
import java.util.Map;

@SpringView(name = LearnView.VIEW_NAME)
public class LearnView extends VerticalLayout implements View {

    public static final String VIEW_NAME = "learn";
    private BeanItemContainer<Lesson> dataSource;
    private final LinkedHashMap<String, String> columns = new LinkedHashMap<>();
    @Autowired
    private LessonService lessonService;


    @PostConstruct
    void init() {
        columns.put("title", "Урок");
        dataSource= new BeanItemContainer(Lesson.class, lessonService.getLessons());
         addComponent( getCustomersGrid());

    }

    private Component getCustomersGrid(){
        VerticalLayout content = new VerticalLayout();
        content.setSizeFull();
        Grid grid = new Grid();
        content.addComponent(grid);
        grid.setSizeFull();
        grid.setColumns(columns.keySet().toArray());
        for(Map.Entry<String, String> row : columns.entrySet()) {
            grid.getColumn(row.getKey()).setHeaderCaption(row.getValue());
        }
        grid.setContainerDataSource(dataSource);

        grid.addSelectionListener(selectionEvent -> { // Java 8
            // Get selection from the selection model
            Object selected = ((Grid.SingleSelectionModel)
                    grid.getSelectionModel()).getSelectedRow();

            if (selected != null){
                Item item =grid.getContainerDataSource().getItem(selected);
                Notification.show("Selected name = \"" +
                        item.getItemProperty("title")+"\" id=\""+
                        item.getItemProperty("id")+"\""
                );
            }
            else
                Notification.show("Nothing selected");
        });
        return  content;
    }



    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {
    }

}

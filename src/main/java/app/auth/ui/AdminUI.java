package app.auth.ui;

import app.auth.service.UserSessionService;
import app.auth.ui.view.MainMenu;
import app.auth.ui.view.admin.LearnView;
import com.vaadin.annotations.Theme;
import com.vaadin.annotations.Title;
import com.vaadin.navigator.Navigator;
import com.vaadin.server.Responsive;
import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.spring.navigator.SpringViewProvider;
import com.vaadin.ui.ComponentContainer;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.UI;
import com.vaadin.ui.themes.ValoTheme;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetailsService;


@Theme("backstage")
@Title("App")
@SpringUI(path = "/admin")
public class AdminUI extends UI {


    @Autowired
    private SpringViewProvider viewProvider;

    @Autowired
    private UserSessionService sessionService;

    @Autowired
    private UserDetailsService userDetailsService;


    private Navigator navigator;

    @Override
    protected void init(VaadinRequest request) {
        Responsive.makeResponsive(this);
        addStyleName(ValoTheme.UI_WITH_MENU);
        setSizeFull();
        addStyleName("mainview");
        HorizontalLayout layout = new HorizontalLayout();
        setContent(layout);
        layout.setSpacing(true);
        layout.setSizeFull();
        layout.addComponent(new MainMenu(sessionService, userDetailsService));
        ComponentContainer content = new CssLayout();
        content.addStyleName("view-content");
        content.setSizeFull();
        layout.addComponent(content);
        layout.setExpandRatio(content, 1.0f);
        navigator = new Navigator(this, content);
        getNavigator().addProvider(viewProvider);
        getNavigator().navigateTo(LearnView.VIEW_NAME);
    }


}

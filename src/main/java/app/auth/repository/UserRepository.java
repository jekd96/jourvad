package app.auth.repository;


import app.auth.domain.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface UserRepository extends CrudRepository<User, Long> {

    User findOneByEmailIgnoringCase(String email);

    User findOneByUsername(String username);

    List<User> findAllByUsername(String username);

}

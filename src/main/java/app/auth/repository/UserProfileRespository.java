package app.auth.repository;

import app.auth.domain.UserProfile;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserProfileRespository extends JpaRepository<UserProfile, Long> {
}

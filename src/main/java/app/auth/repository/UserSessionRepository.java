package app.auth.repository;

import app.auth.domain.User;
import app.auth.domain.UserSession;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;

import java.util.List;


public interface UserSessionRepository extends CrudRepository<UserSession, Long>, JpaSpecificationExecutor<UserSession> {

    UserSession findOneByToken(String token);

    List<UserSession> findByUser(User user);

}

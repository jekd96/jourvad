package app.auth.repository;

import app.auth.domain.Skip;
import org.springframework.data.repository.CrudRepository;

public interface SkipRepostitory extends CrudRepository<Skip, Long> {
}

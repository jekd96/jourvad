package app.auth.repository;


import app.auth.domain.Group;
import app.auth.repository.custom.GroupRepositoryCustom;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GroupRepository extends JpaRepository<Group, Long>, GroupRepositoryCustom {
}

package app.auth.repository.custom;

import app.auth.domain.Student;
import app.auth.dto.StudentDTO;
import app.auth.utils.jooq.tables.Groups;
import app.auth.utils.jooq.tables.Students;
import org.jooq.DSLContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

import static app.auth.utils.jooq.Tables.GROUPS;
import static app.auth.utils.jooq.tables.Students.STUDENTS;

@Repository
public class StudentRepositoryImpl implements StudentRepositoryCustom {

    @Autowired
    public DSLContext dslContext;

    public List<Student> findStudentByGroup(Long groupId) {
        return dslContext.select().from(STUDENTS)
                .where(STUDENTS.GROUP_ID.eq(groupId))
                .fetch()
                .into(Student.class);
    }

}

package app.auth.repository.custom;

import app.auth.domain.Student;
import app.auth.dto.StudentDTO;

import java.util.List;

public interface StudentRepositoryCustom {
    List<Student> findStudentByGroup(Long groupId);
}

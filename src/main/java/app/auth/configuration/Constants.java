package app.auth.configuration;

/**
 * Created by asidorov on 08.06.2016.
 */

public class Constants {

    public static final String DATE_FORMAT = "%1$td.%1$tm.%1$tY";
    public static final String DATETIME_FORMAT = "%1$td.%1$tm.%1$tY %1$tH:%1$tM";
    public static final String DATE_FORMAT_SIMPLE = "dd.MM.yyyy";
    public static final String DATETIME_FORMAT_SIMPLE = "dd.MM.yyyy HH:mm";

    public static final String PERCENT_FORMAT = "#0.00";
    public static final String PRICE_FORMAT = "#0.00";

    public static final String INTEGER_FORMAT = "%1$.0f";
    public static final String TWOFRACTION_FORMAT = "%1$.2f";
}

package app.auth.configuration;

import app.auth.service.AppUserDetailsService;
import app.auth.service.UserSessionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

/**
 * Created by almagnit on 11.09.2016.
 */

@Configuration
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Autowired
    private AppUserDetailsService userDetailsService;

    @Autowired
    private UserSessionService userSessionService;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .csrf().disable()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()
                .exceptionHandling().and()
                .anonymous().and()
                .servletApi().and()
                .authorizeRequests()
                //allow anonymous site root requests
                .antMatchers("/").permitAll()
                //allow anonymous requests to login and signup
                .antMatchers(HttpMethod.GET, "/signin.html").permitAll()
                .antMatchers(HttpMethod.POST, "/login").permitAll()
                .antMatchers(HttpMethod.POST, "/signup").permitAll()

                .antMatchers("/vaadinServlet/**").permitAll()
                .antMatchers("/VAADIN/**").permitAll()
                .antMatchers("/auth/**").permitAll()

                //allow OPTIONS requests
                .antMatchers(HttpMethod.OPTIONS, "/**").permitAll()

                //deny anonymous requests to API
                .antMatchers(HttpMethod.GET, "/api/**").hasAnyRole("ROLE_USER", "ROLE_ADMIN")
                .antMatchers(HttpMethod.POST, "/api/**").hasAnyRole("ROLE_USER", "ROLE_ADMIN")
                .antMatchers(HttpMethod.PUT, "/api/**").hasAnyRole("ROLE_USER", "ROLE_ADMIN")
                .antMatchers(HttpMethod.HEAD, "/api/**").hasAnyRole("ROLE_USER", "ROLE_ADMIN")
                .anyRequest().authenticated()
                .and()
                .formLogin().successForwardUrl("/api/bingo")

                .loginPage("/auth#!signin")
//                .loginPage("/auth")
                .and()


                // custom JSON based authentication by POST of {"username":"<name>","password":"<password>"} which sets the token header upon authentication
                .addFilterBefore(new StatelessLoginFilter("/login", userSessionService, userDetailsService, authenticationManager()), UsernamePasswordAuthenticationFilter.class)

                // custom Token based authentication based on the header previously given to the client
                .addFilterBefore(new StatelessAuthenticationFilter(userSessionService), UsernamePasswordAuthenticationFilter.class);
    }

    @Autowired
    public void registerGlobalAuthentication(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService).passwordEncoder(new BCryptPasswordEncoder());
    }

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder());
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

}

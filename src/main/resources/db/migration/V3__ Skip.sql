CREATE TABLE skips (
   id                     BIGSERIAL PRIMARY KEY ,
   student_id             BIGINT,
   status                 VARCHAR(255),
   subject_id             BIGINT
);

CREATE EXTENSION IF NOT EXISTS "pgcrypto" WITH SCHEMA public;

CREATE TABLE users (
  id                     BIGSERIAL PRIMARY KEY,
  enabled                 BOOLEAN      NOT NULL DEFAULT FALSE,
  email                   VARCHAR(250) NOT NULL,
  username                VARCHAR(250) NOT NULL,
  password                VARCHAR(250) NOT NULL,
  user_profile_id         BIGINT
);

CREATE TABLE user_profiles (
  id                     BIGSERIAL PRIMARY KEY,
  first_name             VARCHAR(250),
  last_name              VARCHAR(250)
  password                VARCHAR(250) NOT NULL,
  user_profile_id         BIGINT
);

CREATE TABLE user_profiles (
  id                      BIGSERIAL PRIMARY KEY,
  first_name              VARCHAR(250),
  last_name               VARCHAR(250)
);

CREATE TABLE authorities (
  id                     BIGSERIAL PRIMARY KEY,
  authority_name      VARCHAR(250) NOT NULL,
  CONSTRAINT unique_authority_name UNIQUE (authority_name)
);

CREATE TABLE user_authorities (
  user_id      BIGINT NOT NULL,
  authority_id BIGINT NOT NULL,
  CONSTRAINT fk_authorities_users FOREIGN KEY (user_id) REFERENCES users (id),
  CONSTRAINT fk_group_authorities_authority FOREIGN KEY (authority_id) REFERENCES authorities (id),
  CONSTRAINT unique_user_authorities UNIQUE (user_id, authority_id)
);

CREATE TABLE user_sessions (
  id                     BIGSERIAL PRIMARY KEY,
  user_id             BIGINT        NOT NULL,
  token               VARCHAR(2500) NOT NULL,
  expires             BIGINT        NOT NULL,
  FOREIGN KEY (user_id) REFERENCES users
);

CREATE TABLE groups (
  id                BIGSERIAL PRIMARY KEY,
  title             VARCHAR(255)    NOT NULL
);

CREATE TABLE group_lessons (
  group_id BIGINT,
  lesson_id BIGINT
);

CREATE TABLE lessons (
  id                BIGSERIAL PRIMARY KEY,
  title             VARCHAR(255)     NOT NULL
);

CREATE TABLE subjects (
   id                     BIGSERIAL PRIMARY KEY ,
   created_date           TIMESTAMPTZ DEFAULT now(),
   topic                  VARCHAR(255),
   teacher_id             BIGINT,
   status                 VARCHAR(255),
   group_id               BIGINT,
   lesson_id              BIGINT
);

CREATE TABLE days (
   id                BIGSERIAL PRIMARY KEY,
   title             VARCHAR(255)    NOT NULL,
   abbreviated       VARCHAR(255)    NOT NULL
);

CREATE TABLE teachers (
  id                BIGSERIAL PRIMARY KEY,
  name              VARCHAR(255)
);

CREATE TABLE teacher_lessons (
  teacher_id BIGINT,
  lesson_id BIGINT
);

CREATE TABLE students (
  id                BIGSERIAL PRIMARY KEY,
  name              VARCHAR(255),
  group_id          BIGINT
);
